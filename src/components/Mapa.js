import React, { Component } from 'react'
import { Map, TileLayer, Marker, Popup } from 'react-leaflet'
import MarkerFriends from './MapaMakers'

export default class MapRender extends Component {
  state = {
    requestLocation: false,
    hasLocation: false,
    latlng: {
      lat: 51.505,
      lng: -0.09,
    },
    amigos:[
      { key: 'marker1', position: [51.5, -0.1], children: 'My first popup' },
      { key: 'marker2', position: [51.51, -0.1], children: 'My second popup' },
      { key: 'marker3', position: [51.49, -0.05], children: 'My third popup' },
    ]
  }
  constructor(props){
    super(props)      
  }
  
  componentDidMount(){
    this.handleClick();
  }
  
  handleClick = () => {
    this.refs.map.leafletElement.locate()
  }

  handleClick2 = (e) => {
    if(!this.state.hasLocation)
     this.setState({
      requestLocation: false,
      hasLocation: true,
      latlng: e.latlng,
    })
    this.reloadAmigos()
  }
  
  handleLocationFound = e => {
    this.setState({
      hasLocation: true,
      latlng: e.latlng,
    })
    this.reloadAmigos();
  }
  
  handleLocationError = e => {
    this.setState({
      requestLocation: true,
    })
  }

  reloadAmigos(){
    this.setState({
      amigos: [
      { key: 'marker1', position: [this.state.latlng.lat +0.01, this.state.latlng.lng -0.02], children: 'My first popup' },
      { key: 'marker2', position: [this.state.latlng.lat +0.02, this.state.latlng.lng -0.01], children: 'My second popup' },
      { key: 'marker3', position: [this.state.latlng.lat +0.01, this.state.latlng.lng +0.001], children: 'My third popup' },
      ]
    })
  }
  
  render() {
    
    const marker = this.state.hasLocation ? (
      <Marker position={this.state.latlng}>
      <Popup>
      <span>Você esta aqui</span>
      </Popup>
      </Marker>
    ) : null
    
    const clickLocation = this.state.requestLocation ? (
      <div className="container">
        <h2>Clique no mapa para definir sua localização.</h2>
      </div>
    ) : null
    
    const amigoNoMapa = this.state.hasLocation ? (
      <MarkerFriends markers={this.state.amigos}/>
    ) : null
    
    return (
      <div>
      {clickLocation}
      <Map
      center={this.state.latlng}
      length={4}
      onClick={this.handleClick2.bind(this)}
      onLocationfound={this.handleLocationFound}
      onLocationError={this.handleLocationError}
      ref="map"
      zoom={13}>
      <TileLayer
      attribution="&amp;copy <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
      url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      {marker}
      {amigoNoMapa}
      </Map>
      
      </div>
    )
  }
}