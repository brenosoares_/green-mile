# Segue as considerações sobre o projeto.

## 1) Uma autenticação do usuário na API do Twitter deve ser feita no primeiro passo: https://developer.twitter.com/
Utilizei o componente TwitterLogin para realização do da autenticação, mas como não domino muito bem a criação de backende em node consegui realizar o login, por um tutorial que encontrei na web, mas n consegui armazenar o token para continuar logado.

## 2) Solicitar a posição do usuário. Caso não seja encontrada nenhuma informação na área, deve ser pedido para se definir uma latitude e longitude.
Utilizei o react-leaflet para renderização do mapa e consegui realizar a solicitação do posição do usuário, seja pela geolocalização ou pelo click no mapa.

## 3) Montar o mapa (dê preferência ao react-leaflet ou Leaflet) e exibir marcadores indicando os twittes, próximos à você (usuário), daquelesque você segue
Aqui tive problemas, pois como não conseguir acessar a API do twitter para buscar os endpoints de usuários, deixei tudo estático, renderizei o marcador da localização atual da pessoa, e alguns ao redor simulando os outros usuários do twitter.

## 4) Ao clicar no marcador, deve ser exibido uma pequena Popup com o texto twittado e a foto e/ou nome do usário do autor do twitte
Não consegui encontrar na documentação do react-leaflet maneiras de renderizar html dentro do popup, então acabei não realizando essa tarefa.

# Para rodar a apalicação tem que rodar o o conteúdo da pasta backend com o comando gulp serve, espera-se tb o mongoDB instalado e rodando com um banco twitter-demo, após isso só rodar npm start na pasta raiz