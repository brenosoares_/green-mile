import React, { Component } from 'react';

import './App.css';

import MapRender from './components/Mapa';
import TwitterLogin from './components/TwitterLogin';

class App extends Component {
  
  constructor() {
    super();
    
    this.state = { isAuthenticated: false, user: null, token: ''};
  }
  
  onSuccess = (response) => {
    const token = response.headers.get('x-auth-token');
    response.json().then(user => {

      console.log(token);
            
      if (token) {
        this.setState({isAuthenticated: true, user: user, token: token});
      }
    });
    
  };
  
  onFailed = (error) => {
    alert(error);
  };
  
  logout = () => {
    this.setState({isAuthenticated: false, token: '', user: null})
  };
  
  render() {
    let content = !!this.state.isAuthenticated ?
    (
      <div className="container">
        <div className="row justify-content-end">
        <div className="col-4">
            <button onClick={this.logout} className="btn btn-danger btn-small" >Log out</button>
          </div>
        </div>
        <MapRender />
      </div>
    ) :
    (
      <div className="container">
        <div className="row justify-content-md-center">
          <div className="col-md-auto">
            <h1>Twitter Map Track</h1>
            <p>Faça o seu login no botão abaixo</p>
            <div className="btnTwitter">
              <TwitterLogin loginUrl="http://localhost:4000/api/v1/auth/twitter"
              onFailure={this.onFailed} onSuccess={this.onSuccess}
              requestTokenUrl="http://localhost:4000/api/v1/auth/twitter/reverse"/>
            </div>
          </div>
        </div>
      </div>
    );
    
    return (
      <div className="App">
      {content}
      </div>
    );
  }
}

export default App;
